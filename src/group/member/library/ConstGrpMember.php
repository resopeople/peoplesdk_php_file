<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\file\group\member\library;



class ConstGrpMember
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_GROUP_ENTITY_FACTORY = 'objGroupEntityFactory';
    const DATA_KEY_FILE_ENTITY_FACTORY = 'objFileEntityFactory';
    const DATA_KEY_GROUP_ENTITY_FACTORY_EXEC_CONFIG = 'tabGroupEntityFactoryExecConfig';
    const DATA_KEY_FILE_ENTITY_FACTORY_EXEC_CONFIG = 'tabFileEntityFactoryExecConfig';



    // Attribute configuration
    const ATTRIBUTE_KEY_ID = 'intAttrId';
    const ATTRIBUTE_KEY_DT_CREATE = 'attrDtCreate';
    const ATTRIBUTE_KEY_DT_UPDATE = 'attrDtUpdate';
    const ATTRIBUTE_KEY_FILE = 'attrFile';
    const ATTRIBUTE_KEY_GROUP = 'attrGroup';
    const ATTRIBUTE_KEY_IS_PUBLIC = 'boolAttrIsPublic';

    const ATTRIBUTE_ALIAS_ID = 'id';
    const ATTRIBUTE_ALIAS_DT_CREATE = 'dt-create';
    const ATTRIBUTE_ALIAS_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_ALIAS_FILE = 'file';
    const ATTRIBUTE_ALIAS_GROUP = 'group';
    const ATTRIBUTE_ALIAS_IS_PUBLIC = 'is-public';

    const ATTRIBUTE_NAME_SAVE_ID = 'id';
    const ATTRIBUTE_NAME_SAVE_DT_CREATE = 'dt-create';
    const ATTRIBUTE_NAME_SAVE_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_NAME_SAVE_FILE = 'file';
    const ATTRIBUTE_NAME_SAVE_GROUP = 'group';
    const ATTRIBUTE_NAME_SAVE_IS_PUBLIC = 'is-public';



    // Sub-action type configuration
    const SUB_ACTION_TYPE_ADMIN = 'admin';
    const SUB_ACTION_TYPE_PROFILE = 'profile';



    // Exception message constants
    const EXCEPT_MSG_GROUP_ENTITY_FACTORY_INVALID_FORMAT =
        'Following group entity factory "%1$s" invalid! It must be a group entity factory object.';
    const EXCEPT_MSG_FILE_ENTITY_FACTORY_INVALID_FORMAT =
        'Following file entity factory "%1$s" invalid! It must be a file entity factory object.';
    const EXCEPT_MSG_GROUP_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the group entity factory execution configuration standard.';
    const EXCEPT_MSG_FILE_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the file entity factory execution configuration standard.';



}