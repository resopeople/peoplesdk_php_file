<?php
/**
 * This class allows to define group member entity factory class.
 * Group member entity factory allows to provide new group member entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\file\group\member\model;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\group\group\model\GroupEntityFactory;
use people_sdk\file\file\model\FileEntityFactory;
use people_sdk\file\group\member\library\ConstGrpMember;
use people_sdk\file\group\member\exception\GroupEntityFactoryInvalidFormatException;
use people_sdk\file\group\member\exception\FileEntityFactoryInvalidFormatException;
use people_sdk\file\group\member\exception\GroupEntityFactoryExecConfigInvalidFormatException;
use people_sdk\file\group\member\exception\FileEntityFactoryExecConfigInvalidFormatException;
use people_sdk\file\group\member\model\GrpMemberEntity;



/**
 * @method null|GroupEntityFactory getObjGroupEntityFactory() Get group entity factory object.
 * @method null|FileEntityFactory getObjFileEntityFactory() Get file entity factory object.
 * @method null|array getTabGroupEntityFactoryExecConfig() Get group entity factory execution configuration array.
 * @method null|array getTabFileEntityFactoryExecConfig() Get file entity factory execution configuration array.
 * @method GrpMemberEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method void setObjGroupEntityFactory(null|GroupEntityFactory $objGroupEntityFactory) Set group entity factory object.
 * @method void setObjFileEntityFactory(null|FileEntityFactory $objFileEntityFactory) Set file entity factory object.
 * @method void setTabGroupEntityFactoryExecConfig(null|array $tabGroupEntityFactoryExecConfig) Set group entity factory execution configuration array.
 * @method void setTabFileEntityFactoryExecConfig(null|array $tabFileEntityFactoryExecConfig) Set file entity factory execution configuration array.
 */
class GrpMemberEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|GroupEntityFactory $objGroupEntityFactory = null
     * @param null|FileEntityFactory $objFileEntityFactory = null
     * @param null|array $tabGroupEntityFactoryExecConfig = null
     * @param null|array $tabFileEntityFactoryExecConfig = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null,
        GroupEntityFactory $objGroupEntityFactory = null,
        FileEntityFactory $objFileEntityFactory = null,
        array $tabGroupEntityFactoryExecConfig = null,
        array $tabFileEntityFactoryExecConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objProvider,
            $objEntityCollection
        );

        // Init group entity factory
        $this->setObjGroupEntityFactory($objGroupEntityFactory);

        // Init file entity factory
        $this->setObjFileEntityFactory($objFileEntityFactory);

        // Init group entity factory execution config
        $this->setTabGroupEntityFactoryExecConfig($tabGroupEntityFactoryExecConfig);

        // Init file entity factory execution config
        $this->setTabFileEntityFactoryExecConfig($tabFileEntityFactoryExecConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstGrpMember::DATA_KEY_GROUP_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstGrpMember::DATA_KEY_GROUP_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstGrpMember::DATA_KEY_FILE_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstGrpMember::DATA_KEY_FILE_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstGrpMember::DATA_KEY_GROUP_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstGrpMember::DATA_KEY_GROUP_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        if(!$this->beanExists(ConstGrpMember::DATA_KEY_FILE_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstGrpMember::DATA_KEY_FILE_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstGrpMember::DATA_KEY_GROUP_ENTITY_FACTORY,
            ConstGrpMember::DATA_KEY_FILE_ENTITY_FACTORY,
            ConstGrpMember::DATA_KEY_GROUP_ENTITY_FACTORY_EXEC_CONFIG,
            ConstGrpMember::DATA_KEY_FILE_ENTITY_FACTORY_EXEC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstGrpMember::DATA_KEY_GROUP_ENTITY_FACTORY:
                    GroupEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstGrpMember::DATA_KEY_FILE_ENTITY_FACTORY:
                    FileEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstGrpMember::DATA_KEY_GROUP_ENTITY_FACTORY_EXEC_CONFIG:
                    GroupEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstGrpMember::DATA_KEY_FILE_ENTITY_FACTORY_EXEC_CONFIG:
                    FileEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => GrpMemberEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => ConstGrpMember::ATTRIBUTE_KEY_ID
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new GrpMemberEntity(
            array(),
            $objValidator,
            $objDateTimeFactory,
            $this->getObjGroupEntityFactory(),
            $this->getObjFileEntityFactory(),
            $this->getTabGroupEntityFactoryExecConfig(),
            $this->getTabFileEntityFactoryExecConfig()
        );

        // Return result
        return $result;
    }



}