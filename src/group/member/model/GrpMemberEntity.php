<?php
/**
 * This class allows to define group member entity class.
 * Group member entity allows to design a specific group member entity,
 * which represents specific group, where (group) members can access on specific file.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\file\group\member\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\group\group\library\ConstGroup;
use people_sdk\group\group\model\GroupEntity;
use people_sdk\group\group\model\GroupEntityFactory;
use people_sdk\file\file\library\ConstFile;
use people_sdk\file\file\model\FileEntity;
use people_sdk\file\file\model\FileEntityFactory;
use people_sdk\file\group\member\library\ConstGrpMember;



/**
 * @property null|integer $intAttrId
 * @property null|string|DateTime $attrDtCreate
 * @property null|string|DateTime $attrDtUpdate
 * @property null|integer|FileEntity $attrFile : file id|entity
 * @property null|integer|GroupEntity $attrGroup : group id|entity
 * @property null|boolean $boolAttrIsPublic
 */
class GrpMemberEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;



    /**
     * DI: Group entity factory instance.
     * @var null|GroupEntityFactory
     */
    protected $objGroupEntityFactory;



    /**
     * DI: File entity factory instance.
     * @var null|FileEntityFactory
     */
    protected $objFileEntityFactory;



    /**
     * DI: Group entity factory execution configuration.
     * @var null|array
     */
    protected $tabGroupEntityFactoryExecConfig;



    /**
     * DI: File entity factory execution configuration.
     * @var null|array
     */
    protected $tabFileEntityFactoryExecConfig;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|DateTimeFactoryInterface $objDateTimeFactory = null
     * @param null|GroupEntityFactory $objGroupEntityFactory = null
     * @param null|FileEntityFactory $objFileEntityFactory = null
     * @param null|array $tabGroupEntityFactoryExecConfig = null
     * @param null|array $tabFileEntityFactoryExecConfig = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null,
        GroupEntityFactory $objGroupEntityFactory = null,
        FileEntityFactory $objFileEntityFactory = null,
        array $tabGroupEntityFactoryExecConfig = null,
        array $tabFileEntityFactoryExecConfig = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;
        $this->objGroupEntityFactory = $objGroupEntityFactory;
        $this->objFileEntityFactory = $objFileEntityFactory;
        $this->tabGroupEntityFactoryExecConfig = $tabGroupEntityFactoryExecConfig;
        $this->tabFileEntityFactoryExecConfig = $tabFileEntityFactoryExecConfig;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
    * @inheritdoc
    */
    protected function getStrBaseKey()
    {
        // Init var
        $intId = $this->getAttributeValue(ConstGrpMember::ATTRIBUTE_KEY_ID);
        $result = (
            (is_int($intId) && ($intId > 0)) ?
                strval($intId) :
                parent::getStrBaseKey()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGrpMember::ATTRIBUTE_KEY_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGrpMember::ATTRIBUTE_ALIAS_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGrpMember::ATTRIBUTE_NAME_SAVE_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGrpMember::ATTRIBUTE_KEY_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGrpMember::ATTRIBUTE_ALIAS_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGrpMember::ATTRIBUTE_NAME_SAVE_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGrpMember::ATTRIBUTE_KEY_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGrpMember::ATTRIBUTE_ALIAS_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGrpMember::ATTRIBUTE_NAME_SAVE_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute file (id|entity)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGrpMember::ATTRIBUTE_KEY_FILE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGrpMember::ATTRIBUTE_ALIAS_FILE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGrpMember::ATTRIBUTE_NAME_SAVE_FILE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute group
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGrpMember::ATTRIBUTE_KEY_GROUP,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGrpMember::ATTRIBUTE_ALIAS_GROUP,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGrpMember::ATTRIBUTE_NAME_SAVE_GROUP,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute is public
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstGrpMember::ATTRIBUTE_KEY_IS_PUBLIC,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstGrpMember::ATTRIBUTE_ALIAS_IS_PUBLIC,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstGrpMember::ATTRIBUTE_NAME_SAVE_IS_PUBLIC,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );

        // Init var
        $result = array(
            ConstGrpMember::ATTRIBUTE_KEY_ID => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => [
                                'is_null',
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return $this->checkIsNew();}
                                    ]
                                ]
                            ],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ],
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return (!$this->checkIsNew());}
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                    ]
                ]
            ],
            ConstGrpMember::ATTRIBUTE_KEY_DT_CREATE => $tabRuleConfigValidDate,
            ConstGrpMember::ATTRIBUTE_KEY_DT_UPDATE => $tabRuleConfigValidDate,
            ConstGrpMember::ATTRIBUTE_KEY_FILE => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ]
                            ],
                            'is-valid-entity' => [
                                [
                                    'type_object',
                                    [
                                        'class_path' => [FileEntity::class]
                                    ]
                                ],
                                [
                                    'validation_entity',
                                    [
                                        'attribute_key' => ConstFile::ATTRIBUTE_KEY_ID
                                    ]
                                ],
                                [
                                    'new_entity',
                                    [
                                        'new_require' => false
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be a valid file ID or entity.'
                    ]
                ]
            ],
            ConstGrpMember::ATTRIBUTE_KEY_GROUP => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ]
                            ],
                            'is-valid-entity' => [
                                [
                                    'type_object',
                                    [
                                        'class_path' => [GroupEntity::class]
                                    ]
                                ],
                                [
                                    'validation_entity',
                                    [
                                        'attribute_key' => ConstGroup::ATTRIBUTE_KEY_ID
                                    ]
                                ],
                                [
                                    'new_entity',
                                    [
                                        'new_require' => false
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be a valid group ID or entity.'
                    ]
                ]
            ],
            ConstGrpMember::ATTRIBUTE_KEY_IS_PUBLIC => $tabRuleConfigValidBoolean
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstGrpMember::ATTRIBUTE_KEY_DT_CREATE:
            case ConstGrpMember::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstGrpMember::ATTRIBUTE_KEY_ID:
            case ConstGrpMember::ATTRIBUTE_KEY_FILE:
            case ConstGrpMember::ATTRIBUTE_KEY_GROUP:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstGrpMember::ATTRIBUTE_KEY_DT_CREATE:
            case ConstGrpMember::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            ((!is_null($objDtFactory)) && is_string($value)) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstGrpMember::ATTRIBUTE_KEY_IS_PUBLIC:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstGrpMember::ATTRIBUTE_KEY_DT_CREATE:
            case ConstGrpMember::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrSaveDt($value) :
                        $value
                );
                break;

            case ConstGrpMember::ATTRIBUTE_KEY_FILE:
                $result = array(
                    ConstFile::ATTRIBUTE_NAME_SAVE_ID => (
                        ($value instanceof FileEntity) ?
                            $value->getAttributeValueSave(ConstFile::ATTRIBUTE_KEY_ID) :
                            $value
                    )
                );
                break;

            case ConstGrpMember::ATTRIBUTE_KEY_GROUP:
                $result = array(
                    ConstGroup::ATTRIBUTE_NAME_SAVE_ID => (
                        ($value instanceof GroupEntity) ?
                            $value->getAttributeValueSave(ConstGroup::ATTRIBUTE_KEY_ID) :
                            $value
                    )
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;
        $objGroupEntityFactory = $this->objGroupEntityFactory;
        $objFileEntityFactory = $this->objFileEntityFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstGrpMember::ATTRIBUTE_KEY_DT_CREATE:
            case ConstGrpMember::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && is_string($value)) ?
                        $objDtFactory->getObjDtFromSave($value, true) :
                        $value
                );
                break;

            case ConstGrpMember::ATTRIBUTE_KEY_FILE:
                $result = $value;
                if((!is_null($objFileEntityFactory)) && is_array($value))
                {
                    $objFileEntity = $objFileEntityFactory->getObjEntity(
                        array(),
                        // Try to select file entity, by id, if required
                        (
                            (!is_null($this->tabFileEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstFile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstFile::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabFileEntityFactoryExecConfig
                                        ) :
                                        $this->tabFileEntityFactoryExecConfig
                                ) :
                                (
                                    isset($value[ConstFile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array(
                                            ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                $value[ConstFile::ATTRIBUTE_NAME_SAVE_ID]
                                        ) :
                                        null
                                )
                        )
                    );
                    if($objFileEntity->hydrateSave($value))
                    {
                        $objFileEntity->setIsNew(false);
                        $result = $objFileEntity;
                    }
                }
                else if(isset($value[ConstFile::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstFile::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            case ConstGrpMember::ATTRIBUTE_KEY_GROUP:
                $result = $value;
                if((!is_null($objGroupEntityFactory)) && is_array($value))
                {
                    $objGroupEntity = $objGroupEntityFactory->getObjEntity(
                        array(),
                        // Try to select group entity, by id, if required
                        (
                            (!is_null($this->tabGroupEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabGroupEntityFactoryExecConfig
                                        ) :
                                        $this->tabGroupEntityFactoryExecConfig
                                ) :
                                (
                                    isset($value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array(
                                            ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                $value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]
                                        ) :
                                        null
                                )
                        )
                    );
                    if($objGroupEntity->hydrateSave($value))
                    {
                        $objGroupEntity->setIsNew(false);
                        $result = $objGroupEntity;
                    }
                }
                else if(isset($value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}