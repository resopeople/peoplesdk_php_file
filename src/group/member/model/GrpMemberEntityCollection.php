<?php
/**
 * This class allows to define group member entity collection class.
 * key => GrpMemberEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\file\group\member\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\file\group\member\model\GrpMemberEntity;



/**
 * @method null|GrpMemberEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(GrpMemberEntity $objEntity) @inheritdoc
 */
class GrpMemberEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return GrpMemberEntity::class;
    }



}