<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\file\group\member\browser\library;



class ConstGrpMemberBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CRIT_EQUAL_ID = 'intAttrCritEqualId';
    const ATTRIBUTE_KEY_CRIT_IN_ID = 'tabAttrCritInId';
    const ATTRIBUTE_KEY_CRIT_START_DT_CREATE = 'attrCritStartDtCreate';
    const ATTRIBUTE_KEY_CRIT_END_DT_CREATE = 'attrCritEndDtCreate';
    const ATTRIBUTE_KEY_CRIT_START_DT_UPDATE = 'attrCritStartDtUpdate';
    const ATTRIBUTE_KEY_CRIT_END_DT_UPDATE = 'attrCritEndDtUpdate';
    const ATTRIBUTE_KEY_CRIT_EQUAL_FILE_ID = 'intAttrCritEqualFileId';
    const ATTRIBUTE_KEY_CRIT_IN_FILE_ID = 'tabAttrCritInFileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_FILE_NAME = 'strAttrCritLikeFileName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_FILE_NAME = 'strAttrCritEqualFileName';
    const ATTRIBUTE_KEY_CRIT_IN_FILE_NAME = 'tabAttrCritInFileName';
    const ATTRIBUTE_KEY_CRIT_IS_FILE_CURRENT_PROFILE = 'boolAttrCritIsFileCurrentProfile';
    const ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_ID = 'intAttrCritEqualGroupId';
    const ATTRIBUTE_KEY_CRIT_IN_GROUP_ID = 'tabAttrCritInGroupId';
    const ATTRIBUTE_KEY_CRIT_LIKE_GROUP_NAME = 'strAttrCritLikeGroupName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_NAME = 'strAttrCritEqualGroupName';
    const ATTRIBUTE_KEY_CRIT_IN_GROUP_NAME = 'tabAttrCritInGroupName';
    const ATTRIBUTE_KEY_CRIT_IS_PUBLIC = 'boolAttrCritIsPublic';
    const ATTRIBUTE_KEY_SORT_ID = 'strAttrSortId';
    const ATTRIBUTE_KEY_SORT_DT_CREATE = 'strAttrSortDtCreate';
    const ATTRIBUTE_KEY_SORT_DT_UPDATE = 'strAttrSortDtUpdate';
    const ATTRIBUTE_KEY_SORT_FILE_ID = 'strAttrSortFileId';
    const ATTRIBUTE_KEY_SORT_FILE_NAME = 'strAttrSortFileName';
    const ATTRIBUTE_KEY_SORT_GROUP_ID = 'strAttrSortGroupId';
    const ATTRIBUTE_KEY_SORT_GROUP_NAME = 'strAttrSortGroupName';

    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ID = 'crit-equal-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_ID = 'crit-in-id';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE = 'crit-start-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE = 'crit-end-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE = 'crit-start-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE = 'crit-end-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_FILE_ID = 'crit-equal-file-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_FILE_ID = 'crit-in-file-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_FILE_NAME = 'crit-like-file-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_FILE_NAME = 'crit-equal-file-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_FILE_NAME = 'crit-in-file-name';
    const ATTRIBUTE_ALIAS_CRIT_IS_FILE_CURRENT_PROFILE = 'crit-is-file-current-profile';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_ID = 'crit-equal-group-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_GROUP_ID = 'crit-in-group-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_GROUP_NAME = 'crit-like-group-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_NAME = 'crit-equal-group-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_GROUP_NAME = 'crit-in-group-name';
    const ATTRIBUTE_ALIAS_CRIT_IS_PUBLIC = 'crit-is-public';
    const ATTRIBUTE_ALIAS_SORT_ID = 'sort-id';
    const ATTRIBUTE_ALIAS_SORT_DT_CREATE = 'sort-dt-create';
    const ATTRIBUTE_ALIAS_SORT_DT_UPDATE = 'sort-dt-update';
    const ATTRIBUTE_ALIAS_SORT_FILE_ID = 'sort-file-id';
    const ATTRIBUTE_ALIAS_SORT_FILE_NAME = 'sort-file-name';
    const ATTRIBUTE_ALIAS_SORT_GROUP_ID = 'sort-group-id';
    const ATTRIBUTE_ALIAS_SORT_GROUP_NAME = 'sort-group-name';



}