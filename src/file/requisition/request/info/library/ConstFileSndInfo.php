<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\file\file\requisition\request\info\library;



class ConstFileSndInfo
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Headers configuration
    const HEADER_KEY_FILE_CONTENT_INCLUDE = 'File-Content-Include';



    // URL arguments configuration
    const URL_ARG_KEY_FILE_CONTENT_INCLUDE = 'file-content-include';



}