<?php
/**
 * This class allows to define file entity class.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\file\file\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\file\file\api\FileInterface;
use liberty_code\file\file\base64\library\ConstBase64File;
use liberty_code\file\file\name\api\NameFileInterface;
use liberty_code\file\file\factory\api\FileFactoryInterface;
use liberty_code\file\file\factory\library\ConstFileFactory;
use liberty_code\file\file\factory\standard\library\ConstStandardFileFactory;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\null_value\library\ConstNullValue;
use people_sdk\library\model\entity\null_value\library\ToolBoxNullValue;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\user_profile\user\library\ConstUserProfile;
use people_sdk\user_profile\user\model\UserProfileEntity;
use people_sdk\user_profile\user\model\UserProfileEntityFactory;
use people_sdk\app_profile\app\library\ConstAppProfile;
use people_sdk\app_profile\app\model\AppProfileEntity;
use people_sdk\app_profile\app\model\AppProfileEntityFactory;
use people_sdk\file\file\library\ConstFile;



/**
 * @property null|integer $intAttrId
 * @property null|string|DateTime $attrDtCreate
 * @property null|string|DateTime $attrDtUpdate
 * @property null|string $strAttrProfileType
 * @property null|string|integer|UserProfileEntity|AppProfileEntity $attrProfile : profile id|entity
 * @property null|string $strAttrName
 * @property null|string $strAttrSourceName
 * @property null|string $strAttrDescription
 * @property null|string $strAttrType : file mime type
 * @property null|integer $intAttrSize : file size in bytes
 * @property null|FileInterface $objAttrFile
 */
class FileEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;



    /**
     * DI: File factory instance.
     * @var FileFactoryInterface
     */
    protected $objFileFactory;



    /**
     * DI: User profile entity factory instance.
     * @var null|UserProfileEntityFactory
     */
    protected $objUserProfileEntityFactory;



    /**
     * DI: Application profile entity factory instance.
     * @var null|AppProfileEntityFactory
     */
    protected $objAppProfileEntityFactory;



    /**
     * DI: User profile entity factory execution configuration.
     * @var null|array
     */
    protected $tabUserProfileEntityFactoryExecConfig;



    /**
     * DI: Application profile entity factory execution configuration.
     * @var null|array
     */
    protected $tabAppProfileEntityFactoryExecConfig;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|DateTimeFactoryInterface $objDateTimeFactory = null
     * @param null|FileFactoryInterface $objFileFactory = null
     * @param null|UserProfileEntityFactory $objUserProfileEntityFactory = null
     * @param null|AppProfileEntityFactory $objAppProfileEntityFactory = null
     * @param null|array $tabUserProfileEntityFactoryExecConfig = null
     * @param null|array $tabAppProfileEntityFactoryExecConfig = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null,
        FileFactoryInterface $objFileFactory = null,
        UserProfileEntityFactory $objUserProfileEntityFactory = null,
        AppProfileEntityFactory $objAppProfileEntityFactory = null,
        array $tabUserProfileEntityFactoryExecConfig = null,
        array $tabAppProfileEntityFactoryExecConfig = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;
        $this->objFileFactory = $objFileFactory;
        $this->objUserProfileEntityFactory = $objUserProfileEntityFactory;
        $this->objAppProfileEntityFactory = $objAppProfileEntityFactory;
        $this->tabUserProfileEntityFactoryExecConfig = $tabUserProfileEntityFactoryExecConfig;
        $this->tabAppProfileEntityFactoryExecConfig = $tabAppProfileEntityFactoryExecConfig;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
    * @inheritdoc
    */
    protected function getStrBaseKey()
    {
        // Init var
        $intId = $this->getAttributeValue(ConstFile::ATTRIBUTE_KEY_ID);
        $result = (
            (is_int($intId) && ($intId > 0)) ?
                strval($intId) :
                parent::getStrBaseKey()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstFile::ATTRIBUTE_KEY_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstFile::ATTRIBUTE_ALIAS_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstFile::ATTRIBUTE_NAME_SAVE_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstFile::ATTRIBUTE_KEY_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstFile::ATTRIBUTE_ALIAS_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstFile::ATTRIBUTE_NAME_SAVE_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstFile::ATTRIBUTE_KEY_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstFile::ATTRIBUTE_ALIAS_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstFile::ATTRIBUTE_NAME_SAVE_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile type
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstFile::ATTRIBUTE_KEY_PROFILE_TYPE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstFile::ATTRIBUTE_ALIAS_PROFILE_TYPE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstFile::ATTRIBUTE_NAME_SAVE_PROFILE_TYPE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile (id|entity)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstFile::ATTRIBUTE_KEY_PROFILE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstFile::ATTRIBUTE_ALIAS_PROFILE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstFile::ATTRIBUTE_NAME_SAVE_PROFILE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstFile::ATTRIBUTE_KEY_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstFile::ATTRIBUTE_ALIAS_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstFile::ATTRIBUTE_NAME_SAVE_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute source name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstFile::ATTRIBUTE_KEY_SOURCE_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstFile::ATTRIBUTE_ALIAS_SOURCE_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstFile::ATTRIBUTE_NAME_SAVE_SOURCE_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute description
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstFile::ATTRIBUTE_KEY_DESCRIPTION,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstFile::ATTRIBUTE_ALIAS_DESCRIPTION,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstFile::ATTRIBUTE_NAME_SAVE_DESCRIPTION,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute type
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstFile::ATTRIBUTE_KEY_TYPE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstFile::ATTRIBUTE_ALIAS_TYPE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstFile::ATTRIBUTE_NAME_SAVE_TYPE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute size
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstFile::ATTRIBUTE_KEY_SIZE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstFile::ATTRIBUTE_ALIAS_SIZE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstFile::ATTRIBUTE_NAME_SAVE_SIZE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute file
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstFile::ATTRIBUTE_KEY_FILE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstFile::ATTRIBUTE_ALIAS_FILE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstFile::ATTRIBUTE_NAME_SAVE_FILE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $getTabRuleConfigValidString = function($boolNotEmptyRequire = false)
        {
            $boolNotEmptyRequire = (is_bool($boolNotEmptyRequire) ? $boolNotEmptyRequire : true);
            $result = array(
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-string' => [
                                'type_string'
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a string'
                    ]
                ]
            );

            if($boolNotEmptyRequire)
            {
                $result[0][1]['rule_config']['is-valid-string'][] = array(
                    'sub_rule_not',
                    [
                        'rule_config' => ['is_empty']
                    ]
                );

                $result[0][1]['error_message_pattern'] = '%1$s must be null or a string not empty';
            }

            return $result;
        };
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );

        // Init var
        $tabProfileType = ConstFile::getTabProfileType();
        $strTabProfileType = implode(', ', array_map(
            function($value) {return sprintf('\'%1$s\'', $value);},
            $tabProfileType
        ));
        $result = array(
            ConstFile::ATTRIBUTE_KEY_ID => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => [
                                'is_null',
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return $this->checkIsNew();}
                                    ]
                                ]
                            ],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ],
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return (!$this->checkIsNew());}
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                    ]
                ]
            ],
            ConstFile::ATTRIBUTE_KEY_DT_CREATE => $tabRuleConfigValidDate,
            ConstFile::ATTRIBUTE_KEY_DT_UPDATE => $tabRuleConfigValidDate,
            ConstFile::ATTRIBUTE_KEY_PROFILE_TYPE => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-null' => [
                                [
                                    'compare_equal',
                                    ['compare_value' => ConstNullValue::NULL_VALUE]
                                ]
                            ],
                            'is-valid-profile-type' => [
                                [
                                    'compare_in',
                                    [
                                        'compare_value' => $tabProfileType
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a valid profile type string (' . $strTabProfileType . ').'
                    ]
                ]
            ],
            ConstFile::ATTRIBUTE_KEY_PROFILE => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-null' => [
                                [
                                    'compare_equal',
                                    ['compare_value' => ConstNullValue::NULL_VALUE]
                                ]
                            ],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ]
                            ],
                            'is-valid-user-profile-entity' => [
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {
                                            $strProfileType = $this->getAttributeValue(ConstFile::ATTRIBUTE_KEY_PROFILE_TYPE, false);
                                            return (
                                                is_string($strProfileType) &&
                                                ($strProfileType == ConstFile::PROFILE_TYPE_USER_PROFILE)
                                            );
                                        }
                                    ]
                                ],
                                [
                                    'type_object',
                                    [
                                        'class_path' => [UserProfileEntity::class]
                                    ]
                                ],
                                [
                                    'validation_entity',
                                    [
                                        'attribute_key' => ConstUserProfile::ATTRIBUTE_KEY_ID
                                    ]
                                ],
                                [
                                    'new_entity',
                                    [
                                        'new_require' => false
                                    ]
                                ]
                            ],
                            'is-valid-app-profile-entity' => [
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {
                                            $strProfileType = $this->getAttributeValue(ConstFile::ATTRIBUTE_KEY_PROFILE_TYPE, false);
                                            return (
                                                is_string($strProfileType) &&
                                                ($strProfileType == ConstFile::PROFILE_TYPE_APP_PROFILE)
                                            );
                                        }
                                    ]
                                ],
                                [
                                    'type_object',
                                    [
                                        'class_path' => [AppProfileEntity::class]
                                    ]
                                ],
                                [
                                    'validation_entity',
                                    [
                                        'attribute_key' => ConstAppProfile::ATTRIBUTE_KEY_ID
                                    ]
                                ],
                                [
                                    'new_entity',
                                    [
                                        'new_require' => false
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a valid user or application profile ID or entity.'
                    ]
                ]
            ],
            ConstFile::ATTRIBUTE_KEY_NAME => $getTabRuleConfigValidString(true),
            ConstFile::ATTRIBUTE_KEY_SOURCE_NAME => $getTabRuleConfigValidString( true),
            ConstFile::ATTRIBUTE_KEY_DESCRIPTION => $getTabRuleConfigValidString(false),
            ConstFile::ATTRIBUTE_KEY_TYPE => $getTabRuleConfigValidString(true),
            ConstFile::ATTRIBUTE_KEY_SIZE => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-integer' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => true
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a positive integer.'
                    ]
                ]
            ],
            ConstFile::ATTRIBUTE_KEY_FILE => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-file' => [
                                [
                                    'type_object',
                                    [
                                        'class_path' => [FileInterface::class]
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a valid file.'
                    ]
                ]
            ]
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstFile::ATTRIBUTE_KEY_DT_CREATE:
            case ConstFile::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstFile::ATTRIBUTE_KEY_ID:
            case ConstFile::ATTRIBUTE_KEY_PROFILE:
            case ConstFile::ATTRIBUTE_KEY_SIZE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstFile::ATTRIBUTE_KEY_DT_CREATE:
            case ConstFile::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            ((!is_null($objDtFactory)) && is_string($value)) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstFile::ATTRIBUTE_KEY_PROFILE_TYPE:
            case ConstFile::ATTRIBUTE_KEY_NAME:
            case ConstFile::ATTRIBUTE_KEY_SOURCE_NAME:
            case ConstFile::ATTRIBUTE_KEY_TYPE:
            case ConstFile::ATTRIBUTE_KEY_FILE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstFile::ATTRIBUTE_KEY_DT_CREATE:
            case ConstFile::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrSaveDt($value) :
                        $value
                );
                break;

            case ConstFile::ATTRIBUTE_KEY_PROFILE_TYPE:
                $result = ToolBoxNullValue::getAttributeValueSaveFormatGet($value);
                break;

            case ConstFile::ATTRIBUTE_KEY_PROFILE:
                $strProfileType = $this->getAttributeValue(ConstFile::ATTRIBUTE_KEY_PROFILE_TYPE);
                $result = (
                    (
                        is_string($strProfileType) &&
                        ($strProfileType == ConstFile::PROFILE_TYPE_USER_PROFILE) &&
                        (($value instanceof UserProfileEntity) || is_int($value))
                    ) ?
                        array(
                            ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID => (
                                ($value instanceof UserProfileEntity) ?
                                    $value->getAttributeValueSave(ConstUserProfile::ATTRIBUTE_KEY_ID) :
                                    $value
                            )
                        ) :
                        (
                            (
                                is_string($strProfileType) &&
                                ($strProfileType == ConstFile::PROFILE_TYPE_APP_PROFILE) &&
                                (($value instanceof AppProfileEntity) || is_int($value))
                            ) ?
                                array(
                                    ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID => (
                                        ($value instanceof AppProfileEntity) ?
                                            $value->getAttributeValueSave(ConstAppProfile::ATTRIBUTE_KEY_ID) :
                                            $value
                                    )
                                ) :
                                ToolBoxNullValue::getAttributeValueSaveFormatGet($value)
                        )
                );
                break;

            case ConstFile::ATTRIBUTE_KEY_FILE:
                $result = (
                    (
                        ($value instanceof FileInterface) &&
                        (!($value instanceof NameFileInterface))
                    ) ?
                        base64_encode($value->getStrContent()) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;
        $objFileFactory = $this->objFileFactory;
        $objUserProfileEntityFactory = $this->objUserProfileEntityFactory;
        $objAppProfileEntityFactory = $this->objAppProfileEntityFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstFile::ATTRIBUTE_KEY_DT_CREATE:
            case ConstFile::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && is_string($value)) ?
                        $objDtFactory->getObjDtFromSave($value, true) :
                        $value
                );
                break;

            case ConstFile::ATTRIBUTE_KEY_PROFILE_TYPE:
                $result = ToolBoxNullValue::getAttributeValueSaveFormatSet($value);
                break;

            case ConstFile::ATTRIBUTE_KEY_PROFILE:
                $strProfileType = $this->getAttributeValue(ConstFile::ATTRIBUTE_KEY_PROFILE_TYPE);
                $result = ToolBoxNullValue::getAttributeValueSaveFormatSet($value);
                // Try to set user profile, if required
                if(is_string($strProfileType) && ($strProfileType == ConstFile::PROFILE_TYPE_USER_PROFILE))
                {
                    if((!is_null($objUserProfileEntityFactory)) && is_array($value))
                    {
                        $objUserProfileEntity = $objUserProfileEntityFactory->getObjEntity(
                            array(),
                            // Try to select user profile entity, by id, if required
                            (
                                (!is_null($this->tabUserProfileEntityFactoryExecConfig)) ?
                                    (
                                        isset($value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                            array_merge(
                                                array(
                                                    ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                        $value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]
                                                ),
                                                $this->tabUserProfileEntityFactoryExecConfig
                                            ) :
                                            $this->tabUserProfileEntityFactoryExecConfig
                                    ) :
                                    (
                                        isset($value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]
                                            ) :
                                            null
                                    )
                            )
                        );
                        if($objUserProfileEntity->hydrateSave($value))
                        {
                            $objUserProfileEntity->setIsNew(false);
                            $result = $objUserProfileEntity;
                        }
                    }
                    else if(isset($value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]))
                    {
                        $result = $value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID];
                    }
                }
                // Try to set application profile, if required
                else if(is_string($strProfileType) && ($strProfileType == ConstFile::PROFILE_TYPE_APP_PROFILE))
                {
                    if((!is_null($objAppProfileEntityFactory)) && is_array($value))
                    {
                        $objAppProfileEntity = $objAppProfileEntityFactory->getObjEntity(
                            array(),
                            // Try to select application profile entity, by id, if required
                            (
                                (!is_null($this->tabAppProfileEntityFactoryExecConfig)) ?
                                    (
                                        isset($value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                            array_merge(
                                                array(
                                                    ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                        $value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]
                                                ),
                                                $this->tabAppProfileEntityFactoryExecConfig
                                            ) :
                                            $this->tabAppProfileEntityFactoryExecConfig
                                    ) :
                                    (
                                        isset($value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]
                                            ) :
                                            null
                                    )
                            )
                        );
                        if($objAppProfileEntity->hydrateSave($value))
                        {
                            $objAppProfileEntity->setIsNew(false);
                            $result = $objAppProfileEntity;
                        }
                    }
                    else if(isset($value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]))
                    {
                        $result = $value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID];
                    }
                }
                break;

            case ConstFile::ATTRIBUTE_KEY_FILE:
                $result = (
                    ((!is_null($objFileFactory)) && is_string($value)) ?
                        $objFileFactory->getObjFile(array(
                            ConstFileFactory::TAB_CONFIG_KEY_TYPE => ConstStandardFileFactory::CONFIG_TYPE_BASE64,
                            ConstBase64File::TAB_CONFIG_KEY_DATA_SOURCE => $value
                        )) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}