<?php
/**
 * This class allows to define file entity collection class.
 * key => FileEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\file\file\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\file\file\model\FileEntity;



/**
 * @method null|FileEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(FileEntity $objEntity) @inheritdoc
 */
class FileEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return FileEntity::class;
    }



}