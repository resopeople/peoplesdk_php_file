<?php
/**
 * This class allows to define file entity multi repository class.
 * File entity multi repository is multi repository,
 * allows to prepare data from file entity, to save in requisition persistence.
 * Specified requisition persistence must be able to use HTTP client, persistor HTTP request and persistor HTTP response, with json and multipart parsing,
 * to handle HTTP request sending and HTTP response reception.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\file\file\model\repository;

use liberty_code\file\file\api\FileInterface;
use people_sdk\library\model\repository\multi\model\MultiRepository;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\parser\parser\factory\library\ConstParserFactory;
use liberty_code\parser\parser\factory\string_table\library\ConstStrTableParserFactory;
use liberty_code\model\repository\library\ConstRepository as BaseConstRepository;
use liberty_code\model\repository\multi\library\ConstMultiRepository;
use liberty_code\requisition\request\factory\library\ConstRequestFactory;
use liberty_code\requisition\response\factory\library\ConstResponseFactory;
use liberty_code\requisition\client\info\library\ConstInfoClient;
use liberty_code\requisition\requester\api\RequesterInterface;
use liberty_code\requisition\requester\client\library\ConstClientRequester;
use liberty_code\requisition\persistence\library\ConstPersistor;
use liberty_code\requisition\persistence\model\DefaultPersistor;
use liberty_code\file\file\factory\api\FileFactoryInterface;
use liberty_code\file\file\factory\library\ConstFileFactory;
use liberty_code\http\multipart\library\ToolBoxMultipart;
use liberty_code\http\parser\string_table\multipart_data\library\ConstMultipartDataParser;
use liberty_code\http\parser\factory\string_table\library\ConstHttpStrTableParserFactory;
use liberty_code\http\file\name\download_response\library\ConstDnlResponseFile;
use liberty_code\http\file\factory\name\library\ConstHttpNameFileFactory;
use liberty_code\http\requisition\request\library\ConstHttpRequest;
use liberty_code\http\requisition\request\data\library\ConstDataHttpRequest;
use liberty_code\http\requisition\request\persistence\library\ConstPersistorHttpRequest;
use liberty_code\http\requisition\request\factory\library\ConstHttpRequestFactory;
use liberty_code\http\requisition\response\model\HttpResponse;
use liberty_code\http\requisition\response\data\library\ConstDataHttpResponse;
use liberty_code\http\requisition\response\persistence\library\ConstPersistorHttpResponse;
use liberty_code\http\requisition\response\factory\library\ConstHttpResponseFactory;
use people_sdk\library\requisition\request\info\library\ConstSndInfo;
use people_sdk\library\requisition\request\info\library\ToolBoxSndInfo;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\library\requisition\response\library\ConstResponse;
use people_sdk\library\model\entity\requisition\response\library\ConstResponseEntity;
use people_sdk\library\model\repository\library\ConstRepository;
use people_sdk\file\file\library\ConstFile;
use people_sdk\file\file\model\FileEntity;



class FileEntityMultiRepository extends MultiRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Requester instance.
     * @var RequesterInterface
     */
    protected $objRequester;



    /**
     * DI: File factory instance.
     * @var FileFactoryInterface
     */
    protected $objFileFactory;



    /**
     * DI: boundary.
     * @var null|string
     */
    protected $strBoundary;



    /**
     * Cached boundary.
     * @var null|string
     */
    protected $strCacheBoundary;
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RequesterInterface $objRequester
     * @param FileFactoryInterface $objFileFactory
     * @param null|string $strBoundary = null
     */
    public function __construct(
        RequesterInterface $objRequester,
        FileFactoryInterface $objFileFactory,
        DefaultPersistor $objPersistor = null,
        SndInfoFactoryInterface $objRequestSndInfoFactory = null,
        $strBoundary = null
    )
    {
        // Init properties
        $this->objRequester = $objRequester;
        $this->objFileFactory = $objFileFactory;
        $this->strBoundary = (
            (is_string($strBoundary) && (trim($strBoundary) !== ''))  ?
                $strBoundary :
                null
        );
        $this->strCacheBoundary = null;

        // Call parent constructor
        parent::__construct(
            $objPersistor,
            $objRequestSndInfoFactory
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get boundary.
     *
     * @return string
     */
    public function getStrBoundary()
    {
        // Init var
        $result = $this->strBoundary;

        // Get boundary (from cache or new), if required
        if(is_null($result))
        {
            $cacheExists = (!is_null($this->strCacheBoundary));
            $result = (
                $cacheExists ?
                    $this->strCacheBoundary :
                    ToolBoxMultipart::getStrNewBoundary()
            );

            // Set on cache, if required
            if(!$cacheExists)
            {
                $this->strCacheBoundary = $result;
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Init var
        $strBoundary = $this->getStrBoundary();
        $strSourcePath = 'source';
        $tabPersistConfig = array(
            ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                ConstRequestFactory::TAB_CONFIG_KEY_TYPE => ConstHttpRequestFactory::CONFIG_TYPE_HTTP_PERSISTOR,
                ConstDataHttpRequest::TAB_CONFIG_KEY_BODY_PARSER_CONFIG => [
                    ConstParserFactory::TAB_CONFIG_KEY_TYPE => ConstHttpStrTableParserFactory::CONFIG_TYPE_HTTP_STR_TABLE_MULTIPART_DATA,
                    ConstMultipartDataParser::TAB_CONFIG_KEY_BOUNDARY => $strBoundary
                ],
                ConstPersistorHttpRequest::TAB_CONFIG_KEY_PATH_SEPARATOR => '/',
                ConstPersistorHttpRequest::TAB_CONFIG_KEY_DATA_ID_KEY => ConstFile::ATTRIBUTE_NAME_SAVE_ID
            ],
            ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                    ConstResponseFactory::TAB_CONFIG_KEY_TYPE => ConstHttpResponseFactory::CONFIG_TYPE_HTTP_PERSISTOR,
                    ConstDataHttpResponse::TAB_CONFIG_KEY_BODY_PARSER_CONFIG => [
                        ConstParserFactory::TAB_CONFIG_KEY_TYPE => ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_JSON
                    ],
                    ConstPersistorHttpResponse::TAB_CONFIG_KEY_PATH_SEPARATOR => '/',
                    ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_ID_KEY => ConstFile::ATTRIBUTE_NAME_SAVE_ID
                ]
            ]
        );
        $tabPersistConfigSet = ToolBoxTable::getTabMerge(
            $tabPersistConfig,
            array(
                ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                    ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                        ConstHttpRequest::TAB_SND_INFO_KEY_HEADER => [
                            ConstSndInfo::HEADER_KEY_CONTENT_TYPE => ToolBoxSndInfo::getStrContentTypeMultipartForm($strBoundary),
                            ConstSndInfo::HEADER_KEY_CONTENT_SRC_PATH => $strSourcePath
                        ]
                    ]
                ]
            )
        );
        $result = array(
            BaseConstRepository::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => FileEntity::class,
            BaseConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE => false,
            ConstMultiRepository::TAB_CONFIG_KEY_ATTRIBUTE_KEY_ID => ConstFile::ATTRIBUTE_KEY_ID,

            // Persistence configuration, for persistence action get
            ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_CONFIG => $tabPersistConfig,
            ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_CONFIG => [
                // Additional persistence configuration, for admin sub-action
                ConstFile::SUB_ACTION_TYPE_ADMIN => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest:: TAB_CONFIG_KEY_ID_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG,
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_ID_KEY => 'crit-equal-id',
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'GET',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/file'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s/0',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ],
                // Additional persistence configuration, for profile sub-action
                ConstFile::SUB_ACTION_TYPE_PROFILE => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest:: TAB_CONFIG_KEY_ID_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG,
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_ID_KEY => 'crit-equal-id',
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'GET',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/file/profile'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s/0',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ]
            ],
            ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_TYPE => ConstFile::SUB_ACTION_TYPE_PROFILE,

            // Persistence configuration, for persistence action create
            ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_CONFIG => $tabPersistConfigSet,
            ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_SUB_ACTION_CONFIG => [
                // Additional persistence configuration, for admin sub-action
                ConstFile::SUB_ACTION_TYPE_ADMIN => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest:: TAB_CONFIG_KEY_CREATE_DATA_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_BODY,
                        ConstPersistorHttpRequest:: TAB_CONFIG_KEY_CREATE_DATA_PATH => sprintf(
                            '%1$s/0',
                            $strSourcePath
                        ),
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'POST',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/file'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_RETURN_STATUS =>
                                ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_REQUIRED,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s/0',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ],
                // Additional persistence configuration, for profile sub-action
                ConstFile::SUB_ACTION_TYPE_PROFILE => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest:: TAB_CONFIG_KEY_CREATE_DATA_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_BODY,
                        ConstPersistorHttpRequest:: TAB_CONFIG_KEY_CREATE_DATA_PATH => sprintf(
                            '%1$s/0',
                            $strSourcePath
                        ),
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'POST',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/file/profile'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_RETURN_STATUS =>
                                ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_REQUIRED,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s/0',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ]
            ],
            ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_SUB_ACTION_TYPE => ConstFile::SUB_ACTION_TYPE_PROFILE,

            // Persistence configuration, for persistence action update
            ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_CONFIG => $tabPersistConfigSet,
            ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_CONFIG => [
                // Additional persistence configuration, for admin sub-action
                ConstFile::SUB_ACTION_TYPE_ADMIN => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest:: TAB_CONFIG_KEY_UPDATE_DATA_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_BODY,
                        ConstPersistorHttpRequest:: TAB_CONFIG_KEY_UPDATE_DATA_PATH => sprintf(
                            '%1$s/0',
                            $strSourcePath
                        ),
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'POST',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/file'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_RETURN_STATUS =>
                                ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_REQUIRED,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s/0',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ],
                // Additional persistence configuration, for profile sub-action
                ConstFile::SUB_ACTION_TYPE_PROFILE => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest:: TAB_CONFIG_KEY_UPDATE_DATA_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_BODY,
                        ConstPersistorHttpRequest:: TAB_CONFIG_KEY_UPDATE_DATA_PATH => sprintf(
                            '%1$s/0',
                            $strSourcePath
                        ),
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'POST',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/file/profile'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_RETURN_STATUS =>
                                ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_REQUIRED,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s/0',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ]
            ],
            ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_TYPE => ConstFile::SUB_ACTION_TYPE_PROFILE,

            // Persistence configuration, for persistence action delete
            ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_CONFIG => $tabPersistConfig,
            ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_SUB_ACTION_CONFIG => [
                // Additional persistence configuration, for admin sub-action
                ConstFile::SUB_ACTION_TYPE_ADMIN => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest:: TAB_CONFIG_KEY_DELETE_ID_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG,
                        ConstPersistorHttpRequest:: TAB_CONFIG_KEY_DELETE_ID_KEY => 'crit-equal-id',
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'DELETE',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/file'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_RETURN_STATUS =>
                                ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_OPTIONAL,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s/0',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ],
                // Additional persistence configuration, for profile sub-action
                ConstFile::SUB_ACTION_TYPE_PROFILE => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest:: TAB_CONFIG_KEY_DELETE_ID_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG,
                        ConstPersistorHttpRequest:: TAB_CONFIG_KEY_DELETE_ID_KEY => 'crit-equal-id',
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'DELETE',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/file/profile'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_RETURN_STATUS =>
                                ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_OPTIONAL,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s/0',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ]
            ],
            ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_SUB_ACTION_TYPE => ConstFile::SUB_ACTION_TYPE_PROFILE
        );

        // Return result
        return $result;
    }



    /**
     * Get file object,
     * from specified id.
     * Response can be returned, if required.
     *
     * Configuration array format:
     * Execution configuration can be provided:
     * @see RequesterInterface::executeRequestConfig() configuration array format.
     *
     * @param integer $intId
     * @param string $strSubActionType = ConstFile::SUB_ACTION_TYPE_PROFILE
     * @param null|array $tabConfig = null
     * @param null|HttpResponse &$objResponse = null
     * @return null|FileInterface
     */
    public function getObjFile(
        $intId,
        $strSubActionType = ConstFile::SUB_ACTION_TYPE_PROFILE,
        array $tabConfig = null,
        HttpResponse &$objResponse = null
    )
    {
        // Init var
        $strSubActionType = (
            (
                is_string($strSubActionType) &&
                in_array($strSubActionType, array(
                    ConstFile::SUB_ACTION_TYPE_ADMIN,
                    ConstFile::SUB_ACTION_TYPE_PROFILE
                ))
            ) ?
                $strSubActionType :
                ConstFile::SUB_ACTION_TYPE_PROFILE
        );
        $strRoute = (
            is_int($intId) ?
                (
                    ($strSubActionType == ConstFile::SUB_ACTION_TYPE_ADMIN) ?
                        sprintf('/file/%1$d/content', $intId) :
                        sprintf('/file/profile/%1$d/content', $intId)
                ) :
                null
        );
        $tabExecConfig = $tabConfig;
        $tabConfig = array(
            ConstClientRequester::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                    ConstResponseFactory::TAB_CONFIG_KEY_TYPE => ConstHttpResponseFactory::CONFIG_TYPE_HTTP
                ]
            ]
        );
        $tabConfig = (
            (!is_null($tabExecConfig)) ?
                ToolBoxTable::getTabMerge($tabExecConfig, $tabConfig):
                $tabConfig
        );
        $objRequestSndInfoFactory = $this->getObjRequestSndInfoFactory();
        /** @var null|HttpResponse $objResponse */
        $objResponse = (
            ((!is_null($strRoute)) && (!is_null($objRequestSndInfoFactory))) ?
                $this
                    ->objRequester
                    ->executeRequestConfig(
                        array(
                            ConstRequestFactory::TAB_CONFIG_KEY_TYPE => ConstHttpRequestFactory::CONFIG_TYPE_HTTP,
                            ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => $objRequestSndInfoFactory->getTabSndInfo(
                                array(
                                    ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'GET',
                                    ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => $strRoute,
                                    ConstHttpRequest::TAB_SND_INFO_KEY_HEADER => [
                                        'Download-Require' => 1
                                    ]
                                )
                            )
                        ),
                        $tabConfig
                    ) :
                null
        );
        $result = (
            (
                (!is_null($objResponse)) &&
                ($objResponse->getIntStatusCode() == 200)
            ) ?
                $this->objFileFactory->getObjFile(array(
                    ConstFileFactory::TAB_CONFIG_KEY_TYPE => ConstHttpNameFileFactory::CONFIG_TYPE_HTTP_NAME_DOWNLOAD_RESPONSE,
                    ConstDnlResponseFile::TAB_CONFIG_KEY_HEADER => $objResponse->getTabHeader(),
                    ConstDnlResponseFile::TAB_CONFIG_KEY_BODY => $objResponse->getStrBody()
                )) :
                null
        );

        // Return result
        return $result;
    }





    // Methods repository
    // ******************************************************************************

    /**
     * Load file object,
     * on specified file entity.
     * Response can be returned, if required.
     *
     * Configuration array format:
     * @see getObjFile() configuration array format.
     *
     * @param FileEntity $objFileEntity
     * @param string $strSubActionType = ConstFile::SUB_ACTION_TYPE_PROFILE
     * @param null|array $tabConfig = null
     * @param null|HttpResponse &$objResponse = null
     * @return boolean
     */
    public function loadFile(
        FileEntity $objFileEntity,
        $strSubActionType = ConstFile::SUB_ACTION_TYPE_PROFILE,
        array $tabConfig = null,
        HttpResponse &$objResponse = null
    )
    {
        // Init var
        $objFile = $this->getObjFile(
            $objFileEntity->getAttributeValue(ConstFile::ATTRIBUTE_KEY_ID),
            $strSubActionType,
            $tabConfig,
            $objResponse
        );

        // Set file, if required
        if($result = (!is_null($objFile)))
        {
            $objFileEntity->setAttributeValue(ConstFile::ATTRIBUTE_KEY_FILE, $objFile);
        }

        // Return result
        return $result;
    }



}


