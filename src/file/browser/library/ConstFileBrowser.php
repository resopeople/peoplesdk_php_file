<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\file\file\browser\library;



class ConstFileBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CRIT_EQUAL_ID = 'intAttrCritEqualId';
    const ATTRIBUTE_KEY_CRIT_IN_ID = 'tabAttrCritInId';
    const ATTRIBUTE_KEY_CRIT_START_DT_CREATE = 'attrCritStartDtCreate';
    const ATTRIBUTE_KEY_CRIT_END_DT_CREATE = 'attrCritEndDtCreate';
    const ATTRIBUTE_KEY_CRIT_START_DT_UPDATE = 'attrCritStartDtUpdate';
    const ATTRIBUTE_KEY_CRIT_END_DT_UPDATE = 'attrCritEndDtUpdate';
    const ATTRIBUTE_KEY_CRIT_IS_PROFILE_DEFINED = 'boolAttrCritIsProfileDefined';
    const ATTRIBUTE_KEY_CRIT_IS_CURRENT_PROFILE = 'boolAttrCritIsCurrentProfile';
    const ATTRIBUTE_KEY_CRIT_EQUAL_PROFILE_TYPE = 'strAttrCritEqualProfileType';
    const ATTRIBUTE_KEY_CRIT_IN_PROFILE_TYPE = 'tabAttrCritInProfileType';
    const ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID = 'intAttrCritEqualUserProfileId';
    const ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID = 'tabAttrCritInUserProfileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN = 'strAttrCritLikeUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN = 'strAttrCritEqualUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN = 'tabAttrCritInUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_ID = 'intAttrCritEqualAppProfileId';
    const ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_ID = 'tabAttrCritInAppProfileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_APP_PROFILE_NAME = 'strAttrCritLikeAppProfileName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_NAME = 'strAttrCritEqualAppProfileName';
    const ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_NAME = 'tabAttrCritInAppProfileName';
    const ATTRIBUTE_KEY_CRIT_LIKE_NAME = 'strAttrCritLikeName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_NAME = 'strAttrCritEqualName';
    const ATTRIBUTE_KEY_CRIT_IN_NAME = 'tabAttrCritInName';
    const ATTRIBUTE_KEY_CRIT_IS_SOURCE_NAME_DEFINED = 'boolAttrCritIsSourceNameDefined';
    const ATTRIBUTE_KEY_CRIT_LIKE_SOURCE_NAME = 'strAttrCritLikeSourceName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SOURCE_NAME = 'strAttrCritEqualSourceName';
    const ATTRIBUTE_KEY_CRIT_IN_SOURCE_NAME = 'tabAttrCritInSourceName';
    const ATTRIBUTE_KEY_CRIT_IS_TYPE_DEFINED = 'boolAttrCritIsTypeDefined';
    const ATTRIBUTE_KEY_CRIT_LIKE_TYPE = 'strAttrCritLikeType';
    const ATTRIBUTE_KEY_CRIT_EQUAL_TYPE = 'strAttrCritEqualType';
    const ATTRIBUTE_KEY_CRIT_IN_TYPE = 'tabAttrCritInType';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SIZE = 'intAttrCritEqualSize';
    const ATTRIBUTE_KEY_CRIT_LESS_SIZE = 'intAttrCritLessSize';
    const ATTRIBUTE_KEY_CRIT_GREATER_SIZE = 'intAttrCritGreaterSize';
    const ATTRIBUTE_KEY_SORT_ID = 'strAttrSortId';
    const ATTRIBUTE_KEY_SORT_DT_CREATE = 'strAttrSortDtCreate';
    const ATTRIBUTE_KEY_SORT_DT_UPDATE = 'strAttrSortDtUpdate';
    const ATTRIBUTE_KEY_SORT_PROFILE_TYPE = 'strAttrSortProfileType';
    const ATTRIBUTE_KEY_SORT_USER_PROFILE_ID = 'strAttrSortUserProfileId';
    const ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN = 'strAttrSortUserProfileLogin';
    const ATTRIBUTE_KEY_SORT_APP_PROFILE_ID = 'strAttrSortAppProfileId';
    const ATTRIBUTE_KEY_SORT_APP_PROFILE_NAME = 'strAttrSortAppProfileName';
    const ATTRIBUTE_KEY_SORT_NAME = 'strAttrSortName';
    const ATTRIBUTE_KEY_SORT_SOURCE_NAME = 'strAttrSortSourceName';
    const ATTRIBUTE_KEY_SORT_TYPE = 'strAttrSortType';
    const ATTRIBUTE_KEY_SORT_SIZE = 'strAttrSortSize';

    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ID = 'crit-equal-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_ID = 'crit-in-id';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE = 'crit-start-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE = 'crit-end-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE = 'crit-start-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE = 'crit-end-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_IS_PROFILE_DEFINED = 'crit-is-profile-defined';
    const ATTRIBUTE_ALIAS_CRIT_IS_CURRENT_PROFILE = 'crit-is-current-profile';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_PROFILE_TYPE = 'crit-equal-profile-type';
    const ATTRIBUTE_ALIAS_CRIT_IN_PROFILE_TYPE = 'crit-in-profile-type';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_ID = 'crit-equal-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_ID = 'crit-in-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_USER_PROFILE_LOGIN = 'crit-like-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_LOGIN = 'crit-equal-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_LOGIN = 'crit-in-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_APP_PROFILE_ID = 'crit-equal-app-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_APP_PROFILE_ID = 'crit-in-app-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_APP_PROFILE_NAME = 'crit-like-app-profile-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_APP_PROFILE_NAME = 'crit-equal-app-profile-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_APP_PROFILE_NAME = 'crit-in-app-profile-name';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_NAME = 'crit-like-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_NAME = 'crit-equal-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_NAME = 'crit-in-name';
    const ATTRIBUTE_ALIAS_CRIT_IS_SOURCE_NAME_DEFINED = 'crit-is-source-name-defined';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_SOURCE_NAME = 'crit-like-source-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SOURCE_NAME = 'crit-equal-source-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_SOURCE_NAME = 'crit-in-source-name';
    const ATTRIBUTE_ALIAS_CRIT_IS_TYPE_DEFINED = 'crit-is-type-defined';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_TYPE = 'crit-like-type';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_TYPE = 'crit-equal-type';
    const ATTRIBUTE_ALIAS_CRIT_IN_TYPE = 'crit-in-type';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SIZE = 'crit-equal-size';
    const ATTRIBUTE_ALIAS_CRIT_LESS_SIZE = 'crit-less-size';
    const ATTRIBUTE_ALIAS_CRIT_GREATER_SIZE = 'crit-greater-size';
    const ATTRIBUTE_ALIAS_SORT_ID = 'sort-id';
    const ATTRIBUTE_ALIAS_SORT_DT_CREATE = 'sort-dt-create';
    const ATTRIBUTE_ALIAS_SORT_DT_UPDATE = 'sort-dt-update';
    const ATTRIBUTE_ALIAS_SORT_PROFILE_TYPE = 'sort-profile-type';
    const ATTRIBUTE_ALIAS_SORT_USER_PROFILE_ID = 'sort-user-profile-id';
    const ATTRIBUTE_ALIAS_SORT_USER_PROFILE_LOGIN = 'sort-user-profile-login';
    const ATTRIBUTE_ALIAS_SORT_APP_PROFILE_ID = 'sort-app-profile-id';
    const ATTRIBUTE_ALIAS_SORT_APP_PROFILE_NAME = 'sort-app-profile-name';
    const ATTRIBUTE_ALIAS_SORT_NAME = 'sort-name';
    const ATTRIBUTE_ALIAS_SORT_SOURCE_NAME = 'sort-source-name';
    const ATTRIBUTE_ALIAS_SORT_TYPE = 'sort-type';
    const ATTRIBUTE_ALIAS_SORT_SIZE = 'sort-size';



}