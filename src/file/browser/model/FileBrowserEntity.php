<?php
/**
 * This class allows to define file browser entity class.
 * File browser entity allows to define attributes,
 * to search file entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\file\file\browser\model;

use people_sdk\library\browser\model\DefaultBrowserEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\browser\library\ToolBoxValidBrowserEntity;
use people_sdk\file\file\library\ConstFile;
use people_sdk\file\file\browser\library\ConstFileBrowser;



/**
 * @property null|integer $intAttrCritEqualId
 * @property null|integer[] $tabAttrCritInId
 * @property null|string|DateTime $attrCritStartDtCreate
 * @property null|string|DateTime $attrCritEndDtCreate
 * @property null|string|DateTime $attrCritStartDtUpdate
 * @property null|string|DateTime $attrCritEndDtUpdate
 * @property null|boolean $boolAttrCritIsProfileDefined
 * @property null|boolean $boolAttrCritIsCurrentProfile
 * @property null|string $strAttrCritEqualProfileType
 * @property null|string[] $tabAttrCritInProfileType
 * @property null|integer $intAttrCritEqualUserProfileId
 * @property null|integer[] $tabAttrCritInUserProfileId
 * @property null|string $strAttrCritLikeUserProfileLogin
 * @property null|string $strAttrCritEqualUserProfileLogin
 * @property null|string[] $tabAttrCritInUserProfileLogin
 * @property null|integer $intAttrCritEqualAppProfileId
 * @property null|integer[] $tabAttrCritInAppProfileId
 * @property null|string $strAttrCritLikeAppProfileName
 * @property null|string $strAttrCritEqualAppProfileName
 * @property null|string[] $tabAttrCritInAppProfileName
 * @property null|string $strAttrCritLikeName
 * @property null|string $strAttrCritEqualName
 * @property null|string[] $tabAttrCritInName
 * @property null|boolean $boolAttrCritIsSourceNameDefined
 * @property null|string $strAttrCritLikeSourceName
 * @property null|string $strAttrCritEqualSourceName
 * @property null|string[] $tabAttrCritInSourceName
 * @property null|boolean $boolAttrCritIsTypeDefined
 * @property null|string $strAttrCritLikeType
 * @property null|string $strAttrCritEqualType
 * @property null|string[] $tabAttrCritInType
 * @property null|integer $intAttrCritEqualSize
 * @property null|integer $intAttrCritLessSize
 * @property null|integer $intAttrCritGreaterSize
 * @property null|string $strAttrSortId
 * @property null|string $strAttrSortDtCreate
 * @property null|string $strAttrSortDtUpdate
 * @property null|string $strAttrSortProfileType
 * @property null|string $strAttrSortUserProfileId
 * @property null|string $strAttrSortUserProfileLogin
 * @property null|string $strAttrSortAppProfileId
 * @property null|string $strAttrSortAppProfileName
 * @property null|string $strAttrSortName
 * @property null|string $strAttrSortSourceName
 * @property null|string $strAttrSortType
 * @property null|string $strAttrSortSize
 */
class FileBrowserEntity extends DefaultBrowserEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array_merge(
            parent::getTabConfig(),
            array(
                // Attribute criteria equal id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is profile defined
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IS_PROFILE_DEFINED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_IS_PROFILE_DEFINED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is current profile
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IS_CURRENT_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_IS_CURRENT_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal profile type
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PROFILE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_PROFILE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in profile type
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_PROFILE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_IN_PROFILE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal application profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in application profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_IN_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like application profile name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal application profile name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in application profile name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_IN_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_IN_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is source name defined
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IS_SOURCE_NAME_DEFINED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_IS_SOURCE_NAME_DEFINED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like source name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SOURCE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_SOURCE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal source name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SOURCE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SOURCE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in source name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_SOURCE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SOURCE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is type defined
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IS_TYPE_DEFINED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_IS_TYPE_DEFINED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like type
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal type
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in type
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_IN_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal size
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SIZE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SIZE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria less than size
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_LESS_SIZE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_LESS_SIZE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria greater than size
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_CRIT_GREATER_SIZE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_CRIT_GREATER_SIZE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort profile type
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_SORT_PROFILE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_SORT_PROFILE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_SORT_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_SORT_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort application profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_SORT_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_SORT_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort application profile name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_SORT_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_SORT_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_SORT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_SORT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort source name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_SORT_SOURCE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_SORT_SOURCE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort type
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_SORT_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_SORT_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort size
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstFileBrowser::ATTRIBUTE_KEY_SORT_SIZE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstFileBrowser::ATTRIBUTE_ALIAS_SORT_SIZE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $getTabRuleConfigValidInteger = function(
            $boolCompareEqualRequire = true,
            $boolNullRequire = true
        )
        {
            $boolCompareEqualRequire = (is_bool($boolCompareEqualRequire) ? $boolCompareEqualRequire : true);
            $boolNullRequire = (is_bool($boolNullRequire) ? $boolNullRequire : true);
            $result = array(
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-valid-integer' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => $boolCompareEqualRequire
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' =>
                            '%1$s must be a ' .
                            ($boolCompareEqualRequire ? '' : 'strict ') .
                            'positive integer.'
                    ]
                ]
            );

            if($boolNullRequire)
            {
                $result[0][1]['rule_config']['is-null'] = array('is_null');
                $result[0][1]['error_message_pattern'] =
                    '%1$s must be null or a ' .
                    ($boolCompareEqualRequire ? '' : 'strict ') .
                    'positive integer.';
            }

            return $result;
        };
        $tabRuleConfigValidId = $getTabRuleConfigValidInteger(false);
        $tabRuleConfigValidTabId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-integer' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => $getTabRuleConfigValidInteger(
                                        false,
                                        false
                                    )
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strict positive integers.'
                ]
            ]
        );
        $tabRuleConfigValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty']
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty'
                ]
            ]
        );
        $tabRuleConfigValidTabString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-string' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => ['is_empty']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strings not empty.'
                ]
            ]
        );
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );
        $tabRuleConfigValidSort = ToolBoxValidBrowserEntity::getTabSortRuleConfig();

        // Init var
        $tabProfileType = ConstFile::getTabProfileType();
        $strTabProfileType = implode(', ', array_map(
            function($value) {return sprintf('\'%1$s\'', $value);},
            $tabProfileType
        ));
        $result = array_merge(
            parent::getTabRuleConfig(),
            array(
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID => $tabRuleConfigValidId,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_ID => $tabRuleConfigValidTabId,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE => $tabRuleConfigValidDate,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE => $tabRuleConfigValidDate,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE => $tabRuleConfigValidDate,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE => $tabRuleConfigValidDate,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IS_PROFILE_DEFINED => $tabRuleConfigValidBoolean,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IS_CURRENT_PROFILE => $tabRuleConfigValidBoolean,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PROFILE_TYPE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-profile-type' => [
                                    [
                                        'compare_in',
                                        [
                                            'compare_value' => $tabProfileType
                                        ]
                                    ]
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be null or a valid profile type string (' . $strTabProfileType . ').'
                        ]
                    ]
                ],
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_PROFILE_TYPE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-iterate-profile-type' => [
                                    [
                                        'type_array',
                                        [
                                            'index_only_require' => true
                                        ]
                                    ],
                                    [
                                        'sub_rule_iterate',
                                        [
                                            'rule_config' => [
                                                [
                                                    'compare_in',
                                                    [
                                                        'compare_value' => $tabProfileType
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be null or an array of valid profile type strings (' . $strTabProfileType . ').'
                        ]
                    ]
                ],
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID => $tabRuleConfigValidId,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN => $tabRuleConfigValidTabString,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_ID => $tabRuleConfigValidId,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_APP_PROFILE_NAME => $tabRuleConfigValidString,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_NAME => $tabRuleConfigValidString,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_NAME => $tabRuleConfigValidTabString,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME => $tabRuleConfigValidString,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME => $tabRuleConfigValidString,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME => $tabRuleConfigValidTabString,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IS_SOURCE_NAME_DEFINED => $tabRuleConfigValidBoolean,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SOURCE_NAME => $tabRuleConfigValidString,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SOURCE_NAME => $tabRuleConfigValidString,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_SOURCE_NAME => $tabRuleConfigValidTabString,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IS_TYPE_DEFINED => $tabRuleConfigValidBoolean,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_TYPE => $tabRuleConfigValidString,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_TYPE => $tabRuleConfigValidString,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_TYPE => $tabRuleConfigValidTabString,
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SIZE => $getTabRuleConfigValidInteger(),
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_LESS_SIZE => $getTabRuleConfigValidInteger(false),
                ConstFileBrowser::ATTRIBUTE_KEY_CRIT_GREATER_SIZE => $getTabRuleConfigValidInteger(),
                ConstFileBrowser::ATTRIBUTE_KEY_SORT_ID => $tabRuleConfigValidSort,
                ConstFileBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE => $tabRuleConfigValidSort,
                ConstFileBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE => $tabRuleConfigValidSort,
                ConstFileBrowser::ATTRIBUTE_KEY_SORT_PROFILE_TYPE => $tabRuleConfigValidSort,
                ConstFileBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_ID => $tabRuleConfigValidSort,
                ConstFileBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN => $tabRuleConfigValidSort,
                ConstFileBrowser::ATTRIBUTE_KEY_SORT_APP_PROFILE_ID => $tabRuleConfigValidSort,
                ConstFileBrowser::ATTRIBUTE_KEY_SORT_APP_PROFILE_NAME => $tabRuleConfigValidSort,
                ConstFileBrowser::ATTRIBUTE_KEY_SORT_NAME => $tabRuleConfigValidSort,
                ConstFileBrowser::ATTRIBUTE_KEY_SORT_SOURCE_NAME => $tabRuleConfigValidSort,
                ConstFileBrowser::ATTRIBUTE_KEY_SORT_TYPE => $tabRuleConfigValidSort,
                ConstFileBrowser::ATTRIBUTE_KEY_SORT_SIZE => $tabRuleConfigValidSort
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_ID:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SIZE:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_LESS_SIZE:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_GREATER_SIZE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_ID:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_array($value)) ?
                                array_values(array_map(
                                    function($value){
                                        return (
                                            (is_string($value) && ctype_digit($value)) ?
                                                intval($value) :
                                                $value
                                        );
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            ((!is_null($objDtFactory)) && is_string($value)) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IS_PROFILE_DEFINED:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IS_CURRENT_PROFILE:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IS_SOURCE_NAME_DEFINED:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IS_TYPE_DEFINED:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PROFILE_TYPE:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_APP_PROFILE_NAME:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_NAME:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SOURCE_NAME:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SOURCE_NAME:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_LIKE_TYPE:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_TYPE:
            case ConstFileBrowser::ATTRIBUTE_KEY_SORT_ID:
            case ConstFileBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE:
            case ConstFileBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE:
            case ConstFileBrowser::ATTRIBUTE_KEY_SORT_PROFILE_TYPE:
            case ConstFileBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_ID:
            case ConstFileBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN:
            case ConstFileBrowser::ATTRIBUTE_KEY_SORT_APP_PROFILE_ID:
            case ConstFileBrowser::ATTRIBUTE_KEY_SORT_APP_PROFILE_NAME:
            case ConstFileBrowser::ATTRIBUTE_KEY_SORT_NAME:
            case ConstFileBrowser::ATTRIBUTE_KEY_SORT_SOURCE_NAME:
            case ConstFileBrowser::ATTRIBUTE_KEY_SORT_TYPE:
            case ConstFileBrowser::ATTRIBUTE_KEY_SORT_SIZE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_PROFILE_TYPE:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_NAME:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_SOURCE_NAME:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IN_TYPE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        ((is_array($value)) ? array_values($value) : $value)
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getAttributeValueQueryFormat($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = $this->getAttributeValueFormatGet($strKey, $value);
                break;

            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IS_PROFILE_DEFINED:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IS_CURRENT_PROFILE:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IS_SOURCE_NAME_DEFINED:
            case ConstFileBrowser::ATTRIBUTE_KEY_CRIT_IS_TYPE_DEFINED:
                $result = (is_bool($value) ? ($value ? 1 : 0) : $value);
                break;

            default:
                $result = parent::getAttributeValueQueryFormat($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}