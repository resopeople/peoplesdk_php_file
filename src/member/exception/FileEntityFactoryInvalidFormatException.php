<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\file\member\exception;

use Exception;
use people_sdk\file\file\model\FileEntityFactory;
use people_sdk\file\member\library\ConstMember;



class FileEntityFactoryInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $factory
     */
	public function __construct($factory)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstMember::EXCEPT_MSG_FILE_ENTITY_FACTORY_INVALID_FORMAT,
            mb_strimwidth(strval($factory), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified factory has valid format.
	 * 
     * @param mixed $factory
	 * @return boolean
	 * @throws static
     */
    public static function setCheck($factory)
    {
        // Init var
        $result =
            // Check valid type, if required
            (
                (is_null($factory)) ||
                ($factory instanceof FileEntityFactory)
            );

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static($factory);
        }
		
		// Return result
		return $result;
    }
	
	
	
}