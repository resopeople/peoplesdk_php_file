<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\file\member\library;



class ConstMember
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_USER_PROFILE_ENTITY_FACTORY = 'objUserProfileEntityFactory';
    const DATA_KEY_FILE_ENTITY_FACTORY = 'objFileEntityFactory';
    const DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG = 'tabUserProfileEntityFactoryExecConfig';
    const DATA_KEY_FILE_ENTITY_FACTORY_EXEC_CONFIG = 'tabFileEntityFactoryExecConfig';



    // Attribute configuration
    const ATTRIBUTE_KEY_ID = 'intAttrId';
    const ATTRIBUTE_KEY_DT_CREATE = 'attrDtCreate';
    const ATTRIBUTE_KEY_DT_UPDATE = 'attrDtUpdate';
    const ATTRIBUTE_KEY_FILE = 'attrFile';
    const ATTRIBUTE_KEY_USER_PROFILE = 'attrUserProfile';

    const ATTRIBUTE_ALIAS_ID = 'id';
    const ATTRIBUTE_ALIAS_DT_CREATE = 'dt-create';
    const ATTRIBUTE_ALIAS_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_ALIAS_FILE = 'file';
    const ATTRIBUTE_ALIAS_USER_PROFILE = 'user-profile';

    const ATTRIBUTE_NAME_SAVE_ID = 'id';
    const ATTRIBUTE_NAME_SAVE_DT_CREATE = 'dt-create';
    const ATTRIBUTE_NAME_SAVE_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_NAME_SAVE_FILE = 'file';
    const ATTRIBUTE_NAME_SAVE_USER_PROFILE = 'user-profile';



    // Sub-action type configuration
    const SUB_ACTION_TYPE_ADMIN = 'admin';
    const SUB_ACTION_TYPE_PROFILE = 'profile';



    // Exception message constants
    const EXCEPT_MSG_USER_PROFILE_ENTITY_FACTORY_INVALID_FORMAT =
        'Following user profile entity factory "%1$s" invalid! It must be an user profile entity factory object.';
    const EXCEPT_MSG_FILE_ENTITY_FACTORY_INVALID_FORMAT =
        'Following file entity factory "%1$s" invalid! It must be a file entity factory object.';
    const EXCEPT_MSG_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the user profile entity factory execution configuration standard.';
    const EXCEPT_MSG_FILE_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the file entity factory execution configuration standard.';



}