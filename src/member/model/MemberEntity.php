<?php
/**
 * This class allows to define member entity class.
 * Member entity allows to design a specific member entity,
 * which represents specific user profile, can access on specific file.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\file\member\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\user_profile\user\library\ConstUserProfile;
use people_sdk\user_profile\user\model\UserProfileEntity;
use people_sdk\user_profile\user\model\UserProfileEntityFactory;
use people_sdk\file\file\library\ConstFile;
use people_sdk\file\file\model\FileEntity;
use people_sdk\file\file\model\FileEntityFactory;
use people_sdk\file\member\library\ConstMember;



/**
 * @property null|integer $intAttrId
 * @property null|string|DateTime $attrDtCreate
 * @property null|string|DateTime $attrDtUpdate
 * @property null|integer|FileEntity $attrFile : file id|entity
 * @property null|integer|UserProfileEntity $attrUserProfile : user profile id|entity
 */
class MemberEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;



    /**
     * DI: User profile entity factory instance.
     * @var null|UserProfileEntityFactory
     */
    protected $objUserProfileEntityFactory;



    /**
     * DI: File entity factory instance.
     * @var null|FileEntityFactory
     */
    protected $objFileEntityFactory;



    /**
     * DI: User profile entity factory execution configuration.
     * @var null|array
     */
    protected $tabUserProfileEntityFactoryExecConfig;



    /**
     * DI: File entity factory execution configuration.
     * @var null|array
     */
    protected $tabFileEntityFactoryExecConfig;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|DateTimeFactoryInterface $objDateTimeFactory = null
     * @param null|UserProfileEntityFactory $objUserProfileEntityFactory = null
     * @param null|FileEntityFactory $objFileEntityFactory = null
     * @param null|array $tabUserProfileEntityFactoryExecConfig = null
     * @param null|array $tabFileEntityFactoryExecConfig = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null,
        UserProfileEntityFactory $objUserProfileEntityFactory = null,
        FileEntityFactory $objFileEntityFactory = null,
        array $tabUserProfileEntityFactoryExecConfig = null,
        array $tabFileEntityFactoryExecConfig = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;
        $this->objUserProfileEntityFactory = $objUserProfileEntityFactory;
        $this->objFileEntityFactory = $objFileEntityFactory;
        $this->tabUserProfileEntityFactoryExecConfig = $tabUserProfileEntityFactoryExecConfig;
        $this->tabFileEntityFactoryExecConfig = $tabFileEntityFactoryExecConfig;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
    * @inheritdoc
    */
    protected function getStrBaseKey()
    {
        // Init var
        $intId = $this->getAttributeValue(ConstMember::ATTRIBUTE_KEY_ID);
        $result = (
            (is_int($intId) && ($intId > 0)) ?
                strval($intId) :
                parent::getStrBaseKey()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstMember::ATTRIBUTE_KEY_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstMember::ATTRIBUTE_ALIAS_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstMember::ATTRIBUTE_NAME_SAVE_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstMember::ATTRIBUTE_KEY_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstMember::ATTRIBUTE_ALIAS_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstMember::ATTRIBUTE_NAME_SAVE_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstMember::ATTRIBUTE_KEY_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstMember::ATTRIBUTE_ALIAS_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstMember::ATTRIBUTE_NAME_SAVE_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute file (id|entity)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstMember::ATTRIBUTE_KEY_FILE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstMember::ATTRIBUTE_ALIAS_FILE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstMember::ATTRIBUTE_NAME_SAVE_FILE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute user profile (id|entity)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstMember::ATTRIBUTE_KEY_USER_PROFILE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstMember::ATTRIBUTE_ALIAS_USER_PROFILE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstMember::ATTRIBUTE_NAME_SAVE_USER_PROFILE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );

        // Init var
        $result = array(
            ConstMember::ATTRIBUTE_KEY_ID => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => [
                                'is_null',
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return $this->checkIsNew();}
                                    ]
                                ]
                            ],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ],
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return (!$this->checkIsNew());}
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                    ]
                ]
            ],
            ConstMember::ATTRIBUTE_KEY_DT_CREATE => $tabRuleConfigValidDate,
            ConstMember::ATTRIBUTE_KEY_DT_UPDATE => $tabRuleConfigValidDate,
            ConstMember::ATTRIBUTE_KEY_FILE => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ]
                            ],
                            'is-valid-entity' => [
                                [
                                    'type_object',
                                    [
                                        'class_path' => [FileEntity::class]
                                    ]
                                ],
                                [
                                    'validation_entity',
                                    [
                                        'attribute_key' => ConstFile::ATTRIBUTE_KEY_ID
                                    ]
                                ],
                                [
                                    'new_entity',
                                    [
                                        'new_require' => false
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be a valid file ID or entity.'
                    ]
                ]
            ],
            ConstMember::ATTRIBUTE_KEY_USER_PROFILE => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ]
                            ],
                            'is-valid-entity' => [
                                [
                                    'type_object',
                                    [
                                        'class_path' => [UserProfileEntity::class]
                                    ]
                                ],
                                [
                                    'validation_entity',
                                    [
                                        'attribute_key' => ConstUserProfile::ATTRIBUTE_KEY_ID
                                    ]
                                ],
                                [
                                    'new_entity',
                                    [
                                        'new_require' => false
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be a valid user profile ID or entity.'
                    ]
                ]
            ]
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstMember::ATTRIBUTE_KEY_DT_CREATE:
            case ConstMember::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstMember::ATTRIBUTE_KEY_ID:
            case ConstMember::ATTRIBUTE_KEY_FILE:
            case ConstMember::ATTRIBUTE_KEY_USER_PROFILE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstMember::ATTRIBUTE_KEY_DT_CREATE:
            case ConstMember::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            ((!is_null($objDtFactory)) && is_string($value)) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstMember::ATTRIBUTE_KEY_DT_CREATE:
            case ConstMember::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrSaveDt($value) :
                        $value
                );
                break;

            case ConstMember::ATTRIBUTE_KEY_FILE:
                $result = array(
                    ConstFile::ATTRIBUTE_NAME_SAVE_ID => (
                        ($value instanceof FileEntity) ?
                            $value->getAttributeValueSave(ConstFile::ATTRIBUTE_KEY_ID) :
                            $value
                    )
                );
                break;

            case ConstMember::ATTRIBUTE_KEY_USER_PROFILE:
                $result = array(
                    ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID => (
                        ($value instanceof UserProfileEntity) ?
                            $value->getAttributeValueSave(ConstUserProfile::ATTRIBUTE_KEY_ID) :
                            $value
                    )
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;
        $objUserProfileEntityFactory = $this->objUserProfileEntityFactory;
        $objFileEntityFactory = $this->objFileEntityFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstMember::ATTRIBUTE_KEY_DT_CREATE:
            case ConstMember::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && is_string($value)) ?
                        $objDtFactory->getObjDtFromSave($value, true) :
                        $value
                );
                break;

            case ConstMember::ATTRIBUTE_KEY_FILE:
                $result = $value;
                if((!is_null($objFileEntityFactory)) && is_array($value))
                {
                    $objFileEntity = $objFileEntityFactory->getObjEntity(
                        array(),
                        // Try to select file entity, by id, if required
                        (
                            (!is_null($this->tabFileEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstFile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstFile::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabFileEntityFactoryExecConfig
                                        ) :
                                        $this->tabFileEntityFactoryExecConfig
                                ) :
                                (
                                    isset($value[ConstFile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array(
                                            ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                $value[ConstFile::ATTRIBUTE_NAME_SAVE_ID]
                                        ) :
                                        null
                                )
                        )
                    );
                    if($objFileEntity->hydrateSave($value))
                    {
                        $objFileEntity->setIsNew(false);
                        $result = $objFileEntity;
                    }
                }
                else if(isset($value[ConstFile::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstFile::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            case ConstMember::ATTRIBUTE_KEY_USER_PROFILE:
                $result = $value;
                if((!is_null($objUserProfileEntityFactory)) && is_array($value))
                {
                    $objUserProfileEntity = $objUserProfileEntityFactory->getObjEntity(
                        array(),
                        // Try to select user profile entity, by id, if required
                        (
                            (!is_null($this->tabUserProfileEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabUserProfileEntityFactoryExecConfig
                                        ) :
                                        $this->tabUserProfileEntityFactoryExecConfig
                                ) :
                                (
                                    isset($value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array(
                                            ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                $value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]
                                        ) :
                                        null
                                )
                        )
                    );
                    if($objUserProfileEntity->hydrateSave($value))
                    {
                        $objUserProfileEntity->setIsNew(false);
                        $result = $objUserProfileEntity;
                    }
                }
                else if(isset($value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}