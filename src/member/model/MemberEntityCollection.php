<?php
/**
 * This class allows to define member entity collection class.
 * key => MemberEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\file\member\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\file\member\model\MemberEntity;



/**
 * @method null|MemberEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(MemberEntity $objEntity) @inheritdoc
 */
class MemberEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return MemberEntity::class;
    }



}