<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\file\member\browser\library;



class ConstMemberBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CRIT_EQUAL_ID = 'intAttrCritEqualId';
    const ATTRIBUTE_KEY_CRIT_IN_ID = 'tabAttrCritInId';
    const ATTRIBUTE_KEY_CRIT_START_DT_CREATE = 'attrCritStartDtCreate';
    const ATTRIBUTE_KEY_CRIT_END_DT_CREATE = 'attrCritEndDtCreate';
    const ATTRIBUTE_KEY_CRIT_START_DT_UPDATE = 'attrCritStartDtUpdate';
    const ATTRIBUTE_KEY_CRIT_END_DT_UPDATE = 'attrCritEndDtUpdate';
    const ATTRIBUTE_KEY_CRIT_EQUAL_FILE_ID = 'intAttrCritEqualFileId';
    const ATTRIBUTE_KEY_CRIT_IN_FILE_ID = 'tabAttrCritInFileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_FILE_NAME = 'strAttrCritLikeFileName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_FILE_NAME = 'strAttrCritEqualFileName';
    const ATTRIBUTE_KEY_CRIT_IN_FILE_NAME = 'tabAttrCritInFileName';
    const ATTRIBUTE_KEY_CRIT_IS_FILE_CURRENT_PROFILE = 'boolAttrCritIsFileCurrentProfile';
    const ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID = 'intAttrCritEqualUserProfileId';
    const ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID = 'tabAttrCritInUserProfileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN = 'strAttrCritLikeUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN = 'strAttrCritEqualUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN = 'tabAttrCritInUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IS_CURRENT_USER_PROFILE = 'boolAttrCritIsCurrentUserProfile';
    const ATTRIBUTE_KEY_SORT_ID = 'strAttrSortId';
    const ATTRIBUTE_KEY_SORT_DT_CREATE = 'strAttrSortDtCreate';
    const ATTRIBUTE_KEY_SORT_DT_UPDATE = 'strAttrSortDtUpdate';
    const ATTRIBUTE_KEY_SORT_FILE_ID = 'strAttrSortFileId';
    const ATTRIBUTE_KEY_SORT_FILE_NAME = 'strAttrSortFileName';
    const ATTRIBUTE_KEY_SORT_USER_PROFILE_ID = 'strAttrSortUserProfileId';
    const ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN = 'strAttrSortUserProfileLogin';

    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ID = 'crit-equal-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_ID = 'crit-in-id';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE = 'crit-start-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE = 'crit-end-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE = 'crit-start-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE = 'crit-end-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_FILE_ID = 'crit-equal-file-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_FILE_ID = 'crit-in-file-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_FILE_NAME = 'crit-like-file-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_FILE_NAME = 'crit-equal-file-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_FILE_NAME = 'crit-in-file-name';
    const ATTRIBUTE_ALIAS_CRIT_IS_FILE_CURRENT_PROFILE = 'crit-is-file-current-profile';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_ID = 'crit-equal-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_ID = 'crit-in-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_USER_PROFILE_LOGIN = 'crit-like-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_LOGIN = 'crit-equal-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_LOGIN = 'crit-in-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IS_CURRENT_USER_PROFILE = 'crit-is-current-user-profile';
    const ATTRIBUTE_ALIAS_SORT_ID = 'sort-id';
    const ATTRIBUTE_ALIAS_SORT_DT_CREATE = 'sort-dt-create';
    const ATTRIBUTE_ALIAS_SORT_DT_UPDATE = 'sort-dt-update';
    const ATTRIBUTE_ALIAS_SORT_FILE_ID = 'sort-file-id';
    const ATTRIBUTE_ALIAS_SORT_FILE_NAME = 'sort-file-name';
    const ATTRIBUTE_ALIAS_SORT_USER_PROFILE_ID = 'sort-user-profile-id';
    const ATTRIBUTE_ALIAS_SORT_USER_PROFILE_LOGIN = 'sort-user-profile-login';



}