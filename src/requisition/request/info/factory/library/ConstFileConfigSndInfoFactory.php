<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\file\requisition\request\info\factory\library;



class ConstFileConfigSndInfoFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_FILE_SUPPORT_TYPE = 'file_support_type';
    const TAB_CONFIG_KEY_FILE_FILE_CONTENT_INCLUDE_CONFIG_KEY = 'file_file_content_include_config_key';

    // Support type configuration
    const CONFIG_SUPPORT_TYPE_URL_ARG = 'url_argument';
    const CONFIG_SUPPORT_TYPE_HEADER = 'header';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the file configuration sending information factory standard.';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get configuration array of support types.
     *
     * @return array
     */
    public static function getTabConfigSupportType()
    {
        // Init var
        $result = array(
            self::CONFIG_SUPPORT_TYPE_URL_ARG,
            self::CONFIG_SUPPORT_TYPE_HEADER
        );

        // Return result
        return $result;
    }



}