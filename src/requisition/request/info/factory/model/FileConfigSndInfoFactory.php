<?php
/**
 * Description :
 * This class allows to define file configuration sending information factory class.
 * File configuration sending information factory uses file values, from source configuration object,
 * to provide HTTP request sending information.
 *
 * File configuration sending information factory uses the following specified configuration:
 * [
 *     Default configuration sending information factory,
 *
 *     file_support_type(optional: got "header", if not found):
 *         "string support type, to set file options parameters.
 *         Scope of available values: @see ConstFileConfigSndInfoFactory::getTabConfigSupportType()",
 *
 *     file_file_content_include_config_key(optional):
 *         "string configuration key, to get file file content include option,
 *         used on file options"
 * ]
 *
 * File configuration sending information factory uses the following values,
 * from specified source configuration object, to get sending information:
 * {
 *     Value <file_file_content_include_config_key>(optional): true / false
 * }
 *
 * Note:
 * -> Configuration support type:
 *     - "url_argument":
 *         Data put on URL arguments array.
 *     - "header":
 *         Data put on headers array.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\file\requisition\request\info\factory\model;

use people_sdk\library\requisition\request\info\factory\config\model\DefaultConfigSndInfoFactory;

use people_sdk\library\requisition\request\info\library\ToolBoxSndInfo;
use people_sdk\library\requisition\request\info\factory\library\ConstSndInfoFactory;
use people_sdk\file\file\requisition\request\info\library\ConstFileSndInfo;
use people_sdk\file\requisition\request\info\factory\library\ConstFileConfigSndInfoFactory;
use people_sdk\file\requisition\request\info\factory\exception\ConfigInvalidFormatException;



class FileConfigSndInfoFactory extends DefaultConfigSndInfoFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstSndInfoFactory::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
	// ******************************************************************************

    /**
     * Get specified support type.
     *
     * @param string $strConfigKey
     * @param string $strDefault = ConstFileConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
     * @return string
     */
    protected function getStrSupportType(
        $strConfigKey,
        $strDefault = ConstFileConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
    )
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strDefault = (is_string($strDefault) ? $strDefault : ConstFileConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER);
        $result = (
            isset($tabConfig[$strConfigKey]) ?
                $tabConfig[$strConfigKey] :
                $strDefault
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured file file content include option,
     * used on file options.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithFileFileContentInclude(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstFileConfigSndInfoFactory::TAB_CONFIG_KEY_FILE_FILE_CONTENT_INCLUDE_CONFIG_KEY]) ?
                $tabConfig[ConstFileConfigSndInfoFactory::TAB_CONFIG_KEY_FILE_FILE_CONTENT_INCLUDE_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstFileConfigSndInfoFactory::TAB_CONFIG_KEY_FILE_SUPPORT_TYPE) ==
                    ConstFileConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithParam(
                    ((intval($value) != 0) ? 1 : 0),
                    ($boolOnHeaderRequired ? ConstFileSndInfo::HEADER_KEY_FILE_CONTENT_INCLUDE : null),
                    ((!$boolOnHeaderRequired) ? ConstFileSndInfo::URL_ARG_KEY_FILE_CONTENT_INCLUDE : null)
                );
            },
            $tabInfo,
            function($value){
                return (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($value) ||
                    is_int($value) ||
                    (is_string($value) && ctype_digit($value))
                );
            }
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabSndInfoEngine()
    {
        // Init var
        $result = $this->getTabSndInfoWithFileFileContentInclude();

        // Return result
        return $result;
    }



}