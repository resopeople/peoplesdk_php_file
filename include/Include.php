<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/file/requisition/request/info/library/ConstFileSndInfo.php');

include($strRootPath . '/src/file/library/ConstFile.php');
include($strRootPath . '/src/file/exception/UserProfileEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/file/exception/AppProfileEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/file/exception/UserProfileEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/file/exception/AppProfileEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/file/model/FileEntity.php');
include($strRootPath . '/src/file/model/FileEntityCollection.php');
include($strRootPath . '/src/file/model/FileEntityFactory.php');
include($strRootPath . '/src/file/model/repository/FileEntityMultiRepository.php');
include($strRootPath . '/src/file/model/repository/FileEntityMultiCollectionRepository.php');

include($strRootPath . '/src/file/browser/library/ConstFileBrowser.php');
include($strRootPath . '/src/file/browser/model/FileBrowserEntity.php');

include($strRootPath . '/src/member/library/ConstMember.php');
include($strRootPath . '/src/member/exception/UserProfileEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/member/exception/FileEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/member/exception/UserProfileEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/member/exception/FileEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/member/model/MemberEntity.php');
include($strRootPath . '/src/member/model/MemberEntityCollection.php');
include($strRootPath . '/src/member/model/MemberEntityFactory.php');
include($strRootPath . '/src/member/model/repository/MemberEntityMultiRepository.php');
include($strRootPath . '/src/member/model/repository/MemberEntityMultiCollectionRepository.php');

include($strRootPath . '/src/member/browser/library/ConstMemberBrowser.php');
include($strRootPath . '/src/member/browser/model/MemberBrowserEntity.php');

include($strRootPath . '/src/group/member/library/ConstGrpMember.php');
include($strRootPath . '/src/group/member/exception/GroupEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/group/member/exception/FileEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/group/member/exception/GroupEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/group/member/exception/FileEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/group/member/model/GrpMemberEntity.php');
include($strRootPath . '/src/group/member/model/GrpMemberEntityCollection.php');
include($strRootPath . '/src/group/member/model/GrpMemberEntityFactory.php');
include($strRootPath . '/src/group/member/model/repository/GrpMemberEntityMultiRepository.php');
include($strRootPath . '/src/group/member/model/repository/GrpMemberEntityMultiCollectionRepository.php');

include($strRootPath . '/src/group/member/browser/library/ConstGrpMemberBrowser.php');
include($strRootPath . '/src/group/member/browser/model/GrpMemberBrowserEntity.php');

include($strRootPath . '/src/requisition/request/info/factory/library/ConstFileConfigSndInfoFactory.php');
include($strRootPath . '/src/requisition/request/info/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/requisition/request/info/factory/model/FileConfigSndInfoFactory.php');