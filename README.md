PeopleSdk_Php_File
==================



Description
-----------

Library contains file components,
to implements file tools, using API file features.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Framework library implementation requirement
--------------------------------------------

1. Library repository: liberty_code/parser: version 1.0

    - String table parser factory implementation:
    
        Persistor HTTP request, and persistor HTTP response instances,
        used via default persistor, provided on repository,
        must be able to use string table parser factory features (or equivalent),
        allowing to provide JSON string table parser instances.
        
2. Library repository: liberty_code/validation: version 1.0

    - Standard rules implementation (or equivalent): 
        
        Validator provided on entities, 
        must contain all standard rules, 
        added on its rule collection.
        
    - Validator rules implementation (or equivalent): 
    
        Validator provided on entities, 
        must contain all validator rules, 
        added on its rule collection.
        Each validator rule must use validator,
        with same implementation as validator provided on entities.

3. Library repository: liberty_code/model: version 1.0

    - Validation entity rules implementation (or equivalent): 
        
        Validator provided on entities, 
        must contain all entity rules, 
        added on its rule collection.
        
4. Library repository: liberty_code/http: version 1.0

    - HTTP string table parser factory implementation:
    
        Persistor HTTP request instances,
        used via default persistor, provided on repository,
        must be able to use HTTP string table parser factory features (or equivalent),
        allowing to provide multipart data string table parser instances.
        
    - HTTP request factory implementation: 
     
        Default persistor and/or requester, provided on repository, 
        must be able to use HTTP request factory features (or equivalent), 
        allowing to provide HTTP request and persistor HTTP request instances.
        
    - HTTP response factory implementation: 
    
        Client used on default persistor and/or requester, provided on repository, 
        must be able to use HTTP response factory features (or equivalent), 
        allowing to provide HTTP response and persistor HTTP response instances.
    
    - HTTP client implementation:
    
        Default persistor and/or requester, provided on repository, 
        must be able to use HTTP client features (or equivalent), 
        to execute HTTP request sending.

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root directory
    
    ```sh
    cd "<project_root_dir_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require people_sdk/file ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_dir_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_dir_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "people_sdk/file": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root directory.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

TODO

---


